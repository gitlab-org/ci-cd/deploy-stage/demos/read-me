# The Demo projects

---

## Demo videos

- [Infrastructure](https://www.youtube.com/watch?v=qIFCCQrmuCE&feature=youtu.be)
- [Platform engineering](https://www.youtube.com/watch?v=8C5aSjpuDHw)
- [Application operator](https://www.youtube.com/watch?v=rrV7uyTentQ)
- [Image signing and verification](https://www.youtube.com/watch?v=rk8SyTTPxTs)

---

## Description

These Demo project want to show how GitLab can be used as a best-in-class application delivery platform for Kubernetes. It consists of several projects:

- [Infra + Platform](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/infra-platform): This projects manages the underlying infrastructure (the infra part) and provides a code-based, self-serve automation to enroll new application projects for delivery (the platform part). These feature mostly rely on the Terraform features of GitLab. [Learn more](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/infra-platform/-/blob/main/README.md)
- [Application delivery project](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/podtato-head-delivery): This project is created automatically when the Application code project is enrolled to the platform and includes all the delivery related logic and manifests following the standards set forth by the platform. [Learn more](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/podtato-head-delivery/-/blob/main/README.md)
- [Application code project](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/podtato-head): This project is created by the software development team. There are no restrictions on its usage from the platform. For application delivery, the project provides containers for the delivery process.

The above setup aims to higlight the flexibility and possibilities of GitLab. One might have a more locked-down setup where the whole flow needs to start in the platform project and both the application code and delivery projects are auto-created.

## Application delivery

Application delivery happens with Flux. Flux is installed to the cluster at creation time (using Terraform). When a project enrolls to the platform, Flux is extended to consume one `entrypoint` OCI image for every environment. These `entrypoint` images are built in the delivery project. The delivery project might include additional Flux artifacts referenced from the `entrypoint` image.

The demo application is comprised of several services. The services are:

- entry: provides the ingress and renders the other services on a single page
- hat: provides the hat
- legs: provides both legs
- arms: provides both arms

## Delivery tracking and troubleshooting

The initial cluster provisioning installs the GitLab agent for Kubernetes and configures the agents to share their connections with the application delivery and code projects allowing cluster insights through the GitLab UI, including reporting about the Flux reconciliation status.

Note: The cluster UI needs to be configured manually for every environment. [Follow this issue to automate it](https://gitlab.com/gitlab-org/gitlab/-/issues/412677).

## Security

- We try to minimize the number of long lived tokens across all the project.
  - The infrastructure provisioning uses OIDC to connect to the cloud
  - There are still a few long-lived tokens
    - We use a single group level access token to manage cross-project resources in Terraform. This token is saved as a masked CI/CD variable in the infra project.
    - The agent registration token is stored in the cluster only
    - Each project receives a project specific deploy token that's stored as a `SealedSecret`
    - Each project receives a project+environment specific cosign key-pair. The public key is stores as a `SealedSecret`, while the private key and password are stored as CI/CD variables under the respective delivery project, configured for the given environment.
- The delivery project builds OCI containers to package Helm charts and Flux resources. These OCI images are signed by cosign, and the signature is verified by Flux in the cluster.

Todo: Add key rotation: https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/read-me/-/issues/10

## ToDo

The issue tracker shows ideas to expand this project. Your upvotes on issues are welcome to signal interest in specific directions.
